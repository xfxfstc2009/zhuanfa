//
//  WebViewController.m
//  Zhuandao
//
//  Created by 裴烨烽 on 2017/1/1.
//  Copyright © 2017年 GiganticWhale. All rights reserved.
//

#import "WebViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>
//导入Keychain依赖库
#import <Security/Security.h>
#import "KeychainUUID.h"
@interface WebViewController()<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *h5WebView;

@end

@implementation WebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];
}

#pragma mark - createWebView
-(void)createWebView{
    if (!self.h5WebView) {
        self.h5WebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20)];
        self.h5WebView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.h5WebView.delegate = self;
        self.h5WebView.backgroundColor = [UIColor whiteColor];
        self.h5WebView.scalesPageToFit = YES;
        
        UIDataDetectorTypes dataDetectorTypes = UIDataDetectorTypeLink;
        self.h5WebView.dataDetectorTypes = dataDetectorTypes;
        
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://xiangzhuankecheng.com"]];
            urlRequest.timeoutInterval = 20.;
            urlRequest.HTTPShouldHandleCookies = YES;
            urlRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
            [self.h5WebView loadRequest:urlRequest];
    }
    [self.view addSubview:self.h5WebView];
}


#pragma mark -UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [self actionDidFinishLoad];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{

}

#pragma mark -actionDidFinishLoad
- (void)actionDidFinishLoad {
    JSContext *context = [self.h5WebView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    NSString *jsManager = [NSString stringWithFormat:@"getDeviceNum('%@')",[self info]];
    [context evaluateScript:jsManager];
}

#pragma mark - 保存信息
-(NSString *)info{
    KeychainUUID *keychain = [[KeychainUUID alloc] init];
    id data = [keychain readUDID];
    return data;
}
@end
