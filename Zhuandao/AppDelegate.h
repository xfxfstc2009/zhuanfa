//
//  AppDelegate.h
//  Zhuandao
//
//  Created by 裴烨烽 on 2016/12/31.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

