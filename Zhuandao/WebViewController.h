//
//  WebViewController.h
//  Zhuandao
//
//  Created by 裴烨烽 on 2017/1/1.
//  Copyright © 2017年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

@property(nonatomic,copy)NSString *transferUrl;

@end
